require 'mysql2'

class Database


		def initialize 
					@db = Mysql2::Client.new(host: "localhost", username: "roshan", password: "P@ssword123", database: "loan")
					puts "Connected!"
				    loantable
			end

			#creating table 


			def loantable 
				@db.query( " CREATE TABLE IF NOT EXISTS loantable 
					( 
						userid INT PRIMARY KEY AUTO_INCREMENT,
						name VARCHAR(255) NOT NULL,
						address VARCHAR(255) NOT NULL,
						phoneno INT NOT NULL,
						email VARCHAR(255) NOT NULL,
						purpose_of_loan  VARCHAR(255) NOT NULL,
						total_loan INT NOT NULL,
						status VARCHAR(255) NOT NULL, 
						date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
						  
						
					)     ")
			end

			# creating new account 


			def createaccount ( name, address, phoneno, email, purpose_of_loan, total_loan ) 
					@db.query( " INSERT INTO loantable ( name, address, phoneno, email, purpose_of_loan, total_loan, status)
						VALUES (
							'#{name}', '#{address}', '#{phoneno}', '#{email}', '#{purpose_of_loan}', '#{total_loan}', 'pending'
						)
						")
						puts " \n "
						puts " \n \n ******* U S E R --- S U C C E S S ********* "
						puts " \n "

						
			end

			#listing out every thing from the database table 

			def list				
			@db.query("SELECT * FROM loantable")
			end
			
			# Showing loan from the table 


			def showloan(name) 
				@db.query( " SELECT * FROM loantable where name = '#{name}' ") 
				
			end

			# Deleting loan from the table 

			def deleteloan(name)
				@db.query( " DELETE  FROM loantable where name = '#{name}' ")
			end 

			# Updating the loan from the table 


			def updateloan( name ) 
				val = @db.query( " SELECT * FROM loantable WHERE name = '#{name}' ") 
				val.each do |key| 
					puts " ######### SELECT TYPE SPECIFIC FIELD YOU WANT TO UPDATE ######### \n \n"
					puts   " --NAME--   ---ADDRESS---   ----PHONENO----     ----EMAIL------ " 
					request = gets.chomp.to_s 
					case request 

					    when "name" 
						puts " Enter new name"
						newname = gets.chomp.to_s
						@db.query( " UPDATE loantable SET name = '#{newname}' 
							WHERE name =  '#{name}' ") 
						puts " Newname has been updated"

						when "address" 
						puts " Enter new address"
						newaddress = gets.chomp.to_s
						@db.query( " UPDATE loantable SET address = '#{newaddress}' 
							WHERE name =  '#{name}' ") 
						puts " Newaddress has been updated"

					    when "phoneno" 
						puts " Enter new phoneno"
						newphoneno = gets.chomp.to_s
						@db.query( " UPDATE loantable SET phoneno = '#{newphoneno}' 
							WHERE name =  '#{name}' ") 
						puts " Newphoneno has been updated"

				        when "email" 
						puts " Enter new email"
						newemail = gets.chomp.to_s
						@db.query( " UPDATE loantable SET email = '#{newemail}' 
							WHERE name =  '#{name}' ") 
						puts " Newemail has been updated"
						end
					end
				end

				        
		    
			
				

			#truncate to delete everything from the table 


			def truncate()
				@db.query( " TRUNCATE TABLE loantable ")
				puts " ******** EVERY THING FROM THE TABLE HAS BEEN CLEARED *****************s"
			end
			
			
			#Depositing money into the table to update the status and clear the loan 


			def deposit ( name, depositamount )
				
				val = @db.query( " SELECT total_loan FROM loantable WHERE name = '#{name}' ") 
				val.each do |key| 

				    if key["total_loan"] >= depositamount
					updatedloan = key["total_loan"] - depositamount
					puts " Your new loantable has been updated "
					@db.query( " UPDATE loantable SET total_loan = '#{updatedloan}' 
					WHERE name =  '#{name}' ") 
				    end 
			    end 
			
				puts "-------------------------------------------------------------------------------"
				puts " YOUR AMOUNT HAS SUCCESSFULLY BEEN DEPOSITED "
		    end
			
			# Setting up status in the records in the table 


			def status ( name )
				val = @db.query( " SELECT total_loan FROM loantable WHERE name = '#{name}' ")
				val.each do |key| 
					if key["total_loan"] == 0 
					@db.query( "UPDATE loantable SET status = 'loan paid' WHERE name = '#{name}'")
					puts " LOAN CLEARED "
					else 
					puts " LOAN NOT CLEARED"

					end
		
				end
			end
		   
			def loantime(name) 
				val = @db.query( " SELECT date_time FROM loantable WHERE name = '#{name}' ")
				val.each do |key| 

				print " \n \n #{name} HAS TAKEN LOAN AT " 
				puts key["date_time"]
				puts " \n \n \n "
				end
			end



end


