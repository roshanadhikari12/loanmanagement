# Loan Management System 
The loan management system was created as a milestone project. The loan management is very basic and works in the command line interface.
Loan management system basically has records of the loan users and the deposit amount of the users. Loan management system also integrates in providing
loans according to the user input. 

### Tools Used 
- Ruby
- RVM( as a version control ) 
- pry gem ( to test the codes ) 
- mysql2 
- require gem 

### Tasks of the loan management 
- to create customertable
- to update customertable 
- to delete rows in customertable 
- to deposit money 
- to show time of the deposit 
- to clear the whole records 
- show loan status 
- show loans according to the name 


*the codes are still on the development phase futher codes can be added to make this project more complicated.

### License 

MIT 